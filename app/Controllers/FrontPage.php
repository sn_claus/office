<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use Sober\Controller\Module\Acf;

class FrontPage extends Controller {

    protected $acf = true;
// all returned values from Acf, even if multidimensional arrays are in object notation 

public static function our_service() {
    return get_field('our_service');
}
public function about_us() {
    return(object) [
        'text' => get_field('about_text'),
        'link' => get_field('video_link')
    ];
}
public function offers() {
    return(object) [
        'text' => get_field('offers_text'),
        'items' => get_field('offers_item')
    ];
}
public function tax() {
    return(object) [
        'text' => get_field('tax_text')
    ];
}
public function contacts() {
    return(object) [
        'place' => get_field('place', 'options'),
        'phone' => get_field('phone','options'),
        'mail' => get_field('mail','options')
    ];
}
// public function text()
//     {
//         return Acf::get('text_field');
//         // gets 1 field and returns flattened
//         // use $text in view
//     }

// public function combo()
//     {
//         return Acf::get(['repeater_field', 'text_field']);
//         // gets 2 fields, and returns in object notation using advanced custom fields key values
//         // use $combo->text_field in view
//         // use $combo->repeater_field in view
//     }


}
