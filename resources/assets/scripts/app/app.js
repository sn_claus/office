import toArray from 'lodash/toArray';

export default class App {
  constructor() {
    this._components = {};
  }

  registerComponents() {
    const requireComponent = require.context('./components', true, /\.js$/);

    this._components = requireComponent.keys().reduce((acc, filename) => {
      const Component = requireComponent(filename);
      const [, name] = filename.match(/([\w\-]+)(?:\.lib\.\w+)$/);

      acc[name] = {
        clazz: Component.default,
        instances: []
      };

      return acc;
    }, {});
  }

  initialize() {
    for (const name in this._components) {
      const Component = this._components[name].clazz;
      const selector = Component.selector;

      if (typeof selector !== 'undefined' && typeof selector !== 'string') {
        throw new TypeError('Component selector should be a string');
      }

      const nodes = toArray(document.querySelectorAll(selector));

      this._components[name].instances = nodes.map(node => {
        const instance = new Component(node);
        instance.initialize();
        return instance;
      });
    }
  }
}
