import isString from 'lodash/isString';
import isArray from 'lodash/isArray';
import toArray from 'lodash/toArray';
import listen from 'dom-helpers/events/listen';

export default class Component {
  /**
   * Component's selector
   * @type {string}
   * @static
   * @interface
   */
  static get selector() {}

  /**
   * Create a component
   * @param {HTMLElement} host
   */
  constructor(host) {
    this.host = host;
  }

  /**
   * Find element
   * @param {string} selector
   * @param {HTMLElement|HTMLDocument} [context=this.host]
   * @returns {(HTMLElement|Array<HTMLElement>)}
   */
  find(selector, context = this.host) {
    const nodes = toArray(context.querySelectorAll(selector));

    if (typeof nodes[0] === 'undefined') {
      return null;
    }

    return nodes.length > 1 ? nodes : nodes[0];
  }

  /**
   * Attach event to DOM node
   * @param {(string|HTMLElement|Array<HTMLElement>)} target
   * @param {string} eventName
   * @param {Function} handler
   * @returns {Function} Function that calls "off" hook
   */
  createEvent(target, eventName, handler) {
    target = isString(target)
      ? toArray(this.host.querySelectorAll(target))
      : target;

    const listeners = isArray(target)
      ? target.map(node => listen(node, eventName, handler))
      : [listen(target, eventName, handler)];

    return () => listeners.forEach(off => off());
  }

  /**
   * Function called when component is initialized
   * @interface
   */
  initialize() {}
}
