import Component from '../components';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import IMask from 'imask';
export default class Calclulate extends Component {
    static get selector() {
      return '.trade-tax .form-init';
    }
  
    // values = [];
  
    constructor(host) {
      super(host);
      this.config = {
        mask: '€ num',
        blocks: {
          num: {
            // nested masks are available!
            mask: Number,
            thousandsSeparator: ' '
          }
        }
      };
      this.percent = {
        mask: 'num%',
        blocks: {
          num: {
            // nested masks are available!
            mask: Number,
            min: 200,
            max: 900,
          }
        },
        prepare: function (str) {
          if(str != str) {
            // console.log('wo');
          }
          else {
            // console.log(str);
          }
          return str;
          // return str.toUpperCase();
        },
        commit: function (value, masked) {
          if(!masked.value.match(/%/) && masked.value != '') {
            console.log('where is percent?');
            masked.value = masked.value + '%'
          }
        }
      };
      this.calc_value = this.find("#form_inp");
      this.calc_value = IMask(this.calc_value, this.config);
      this.calc_value2 = this.find("#form_inp2");
      this.calc_value2 = IMask(this.calc_value2, this.percent);

      this.receive_inp = document.querySelector("#calc_inp");
      this.receive_inp = IMask(this.receive_inp, this.config);

      this.receive_inp2 = document.querySelector("#trade_outp2");
      this.receive_inp2 = IMask(this.receive_inp2, this.percent);

      this.local_val = 0;
      this.createEvent(this.find('.btn'), 'click', this.share_calculate.bind(this));
      this.createEvent(this.find('input'), 'change', this.tax_disable.bind(this));//keyup
      // console.log(this.calc_value.input)
      this.createEvent(this.calc_value.el.input, 'keyup', this.tax_disable.bind(this));
      this.createEvent(this.receive_inp.el.input, 'keyup', this.tax_calculate.bind(this));
      
    }
    share_calculate() {
      this.local_val = this.calc_value.unmaskedValue;
      this.local_taxt = this.calc_value2.unmaskedValue;
      this.receive_inp.unmaskedValue = this.calc_value.unmaskedValue;
      this.receive_inp2.value = this.calc_value2.value;
      this.tax_calculate();
      this.local_val = 0;
    }
    tax_calculate() {
      if(this.local_val === 0) {
        this.local_val = this.receive_inp.unmaskedValue;
      }
      var checked = this.find("input:checked");
      
      var checker;
      switch (checked.value) {
        case 'first':
          checker = 24500;
          break;
        case 'second':
          checker = 0;
          break;
        case 'third':
          checker = 5000;
          break;
      }
      
      var calc_outp1 = document.querySelector('#calc_outp1');
      calc_outp1.value = checker;
      calc_outp1 = IMask(calc_outp1, this.config);

      var value = (+this.local_val - checker);

      var calc_outp2 = document.querySelector('#calc_outp2');
      (value > 0 ? calc_outp2.value = value : calc_outp2.value = 0);
      calc_outp2 = IMask(calc_outp2, this.config);
      var tax1 = value * 0.035;
      var tx2 = this.receive_inp2.unmaskedValue / 100;
      var tax2 = tax1 * tx2;
      var tax3 = tax1 * 2.4;

      var result_outp1 = document.querySelector('#result_outp1');
      result_outp1 = IMask(result_outp1, this.config);
      (tax1 > 0 ? result_outp1.unmaskedValue = tax1.toFixed(2) : result_outp1.unmaskedValue = 0);
      
      
      var result_outp2 =  document.querySelector('#result_outp2');
      result_outp2 = IMask(result_outp2, this.config);
      (tax2 > 0 ? result_outp2.unmaskedValue = tax2.toFixed(2) : result_outp2.unmaskedValue = 0);
      
      var result_outp3 =  document.querySelector('#result_outp3');
      result_outp3 = IMask(result_outp3, this.config);
      (tax3 > 0 ? result_outp3.unmaskedValue = tax3.toFixed(2) : result_outp3.unmaskedValue = 0);


      this.local_val = 0;
      // alert('fired');
    }
    tax_disable() {
      var checked = this.find("input:checked");
      if ((this.calc_value.unmaskedValue != '' && this.calc_value2.unmaskedValue != '') &&  checked) {
        this.find('.btn').disabled = false;
      }
      else {
        this.find('.btn').disabled = true;
      }
    }

      initialize() {
  
    }
  }
  