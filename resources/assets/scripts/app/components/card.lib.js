import Component from '../components';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import hasClass from 'dom-helpers/class/hasClass';
import 'vanilla-tilt';
export default class Card extends Component {
    static get selector() {
      return '.offers-content .item';
    }
   
    // data-tilt data-tilt-speed="500"  data-tilt-scale="1.05" data-tilt-max="7" data-tilt-glare data-tilt-max-glare="0.3" data-tilt-reverse="true"
    constructor(host) {
        super(host);

        }
    initialize() {
        var media = window.matchMedia("(min-width: 991px)");
        if(media.matches) {
            VanillaTilt.init(this.host.querySelector(".item-wrap"), {
                speed: 350,
                scale: 1.05,
                max: 7,
                // glare: true,
                // "max-glare": 0.3,
                // reverse: true,
                easing: "ease-in"
            });
        }
        
    }
}