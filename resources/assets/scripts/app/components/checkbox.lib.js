import Component from '../components';
import createEventBus from '../eventBus';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import {MDCFormField} from '@material/form-field';
import {MDCCheckbox} from '@material/checkbox';
export default class Checkbox extends Component {
    static get selector() {
        return '.radio-group';
      }
      
      constructor(host) {
        super(host);
        const checkbox = new MDCCheckbox(document.querySelector('.mdc-checkbox'));
        const formField = new MDCFormField(document.querySelector('.mdc-form-field'));
        formField.input = checkbox;
      }
      initialize() {

      }
}