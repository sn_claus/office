import Component from '../components';
import tippy from 'tippy.js';

export default class CopyTool extends Component {
    static get selector() {
      return '.table-content';
    }
   
    
    constructor(host) {
        super(host);
        function copyTextToClipboard(text) {
            var textArea = document.createElement("textarea");
            textArea.style.position = 'fixed';
            textArea.style.top = 0;
            textArea.style.left = 0;
            textArea.style.width = '2em';
            textArea.style.height = '2em';
            textArea.style.padding = 0;
            textArea.style.border = 'none';
            textArea.style.outline = 'none';
            textArea.style.boxShadow = 'none';
            textArea.style.background = 'transparent';
            textArea.value = text;
            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
            } catch (err) {
                console.log('Oops, unable to copy');
            }

            document.body.removeChild(textArea);
        }


    var copyBobBtns = document.querySelectorAll('.user-partnership .table-content');

    for (var i = 0, len = copyBobBtns.length; i < len; i++) {
        copyBobBtns[i].addEventListener('click', function(event) {
            if (event.target.matches('.link-btn')) {
            copyTextToClipboard(event.target.innerText);
            }
        });
    }
    }
    initialize() {
        tippy('.table-content', {
            target: '.link-btn',
            trigger: 'click',
            hideOnClick: true,
            interactive: true,
            arrow: true,
            animation: 'scale',
            inertia: true,
            theme: 'custom',
            content: 'Ссылка скопирована',
            onShow(element) {
                console.log(element.state);
                this.autoHide = setTimeout(function () {
                    element.hide();
                }, 1500);
            }
        });
    }
}