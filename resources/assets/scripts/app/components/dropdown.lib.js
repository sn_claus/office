import Component from '../components';
import Scrollbar from 'smooth-scrollbar';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import hasClass from 'dom-helpers/class/hasClass';
import toArray from 'lodash/toArray';
import popper from 'popper.js';
export default class Dropdown extends Component {
  static get selector() {
    return '.dropdown';
  }

  // values = [];

  constructor(host) {
    super(host);

    this.toggle = this.find('.dropdown-toggle');
    this.menu = this.find('.dropdown-menu');
    this.input = this.find('input');
    this.parent = host.parentElement;

    this.createEvent(this.menu, 'click', this.onMenuClick.bind(this));
    this.createEvent(this.toggle, 'click', this.onToggleClick.bind(this));
  }

  /**
   * Set value of clicked item
   * @param {MouseEvent} event
   * @param {HTMLElement} event.target
   */
  onMenuClick({ target }) {
    if (hasClass(target, 'dropdown-item') && !hasClass(target, 'dissabled')) {
      const value = target.dataset.value;
      const textElem = this.toggle.querySelector('span');

      if (this.values.indexOf(value) > -1) {
        textElem.innerText = value;
        this.input.value = value;
        addClass(this.host, 'is-filled');
        addClass(this.parent, 'focused');
      }
    }
  }

  /**
   * Toggle dropdown
   */
  onToggleClick() {
    if (hasClass(this.menu, 'd-none')) {
      removeClass(this.menu, 'd-none');
      addClass(this.toggle, 'is-active');
      addClass(this.parent, 'focused');
    } else {
      addClass(this.menu, 'd-none');
      removeClass(this.toggle, 'is-active');
      removeClass(this.parent, 'focused');
    }

    const off = this.createEvent(document, 'mouseup', () => {
      addClass(this.menu, 'd-none');
      removeClass(this.toggle, 'is-active');
      removeClass(this.parent, 'focused');
      off();
    });
  }

  initialize() {
    this.values = toArray(this.host.querySelectorAll('.dropdown-item')).map(
      node => node.dataset.value
    );
    Scrollbar.init(this.menu);
  //   var popper = new Popper(this.toggle, this.menu, {
  //     placement: 'bottom',
  //     preventOverflow: {
  //       boundariesElement: this.host.parentElement,
  //   },
  // });
  }
}
