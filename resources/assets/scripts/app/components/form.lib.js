import Component from '../components';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import hasClass from 'dom-helpers/class/hasClass';

export default class Form extends Component {
    static get selector() {
      return '.form-init:not(.simulate)';
    }
  
    // values = [];
  
    constructor(host) {
      super(host);
      this.inputs = this.find('.input-wrap input:not([type="submit"]),.form-init textarea');
      this.createEvent(this.inputs, 'focusin', this.focusIn.bind(this));
      this.createEvent(this.inputs, 'focusout', this.focusOut.bind(this));
      this.createEvent(this.inputs, 'change', this.focusOut.bind(this));
      document.addEventListener( 'wpcf7invalid', this.invalid.bind(this));
    }
    focusIn({ target }) {
        this.input = target.closest('.input-wrap');
        addClass(this.input, 'focused');
        removeClass(this.input, 'invalid');
    }
    invalid() {
        var inv = document.querySelectorAll('.wpcf7-not-valid');
        for (var i = 0, len = inv.length; i < len; i++) {
            this.input = inv[i].closest('.input-wrap');
            addClass(this.input, 'filled');
            addClass(this.input, 'invalid');
        }
    }
    focusOut({ target }) {
        if(!target.value) {
            this.input = target.closest('.input-wrap');
            removeClass(this.input, 'focused');
            removeClass(this.input, 'filled');
        }
        else if(target.value) {
            this.input = target.closest('.input-wrap');
            removeClass(this.input, 'focused');
            addClass(this.input, 'filled');
        }
    }
}
