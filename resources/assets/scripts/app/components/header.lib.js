import Component from '../components';
import hasClass from 'dom-helpers/class/hasClass';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import Gumshoe from 'gumshoejs';
export default class Header extends Component {
    static get selector() {
      return 'header.main-head';
    }
    constructor(host) {
      super(host);
      const self = this;
      const menu = document.querySelector(".nav-menu ul");
      this.indicator = document.createElement('div');
      this.indicator.classList.add('menu-ink');
      menu.append(this.indicator);

      // this.position_indicator(menu.querySelector("li.active"));  
      // setTimeout(function(){indicator.css("opacity", 1);}, 500);

      var links = menu.querySelectorAll('li');

      for (var i = 0, len = links.length; i < len; i++) {
        links[i].addEventListener('mouseover', function() {
          self.position_indicator(this);
          addClass(this, 'mouseover');
        });
        links[i].addEventListener('mouseout', function() {
          removeClass(this, 'mouseover');
          self.position_indicator(menu.querySelector("li.active"));
        });
      }
      document.addEventListener('gumshoeActivate', function (event) {
        if(!menu.querySelector("li.mouseover")) {
          self.position_indicator(menu.querySelector("li.active"));   
        } 
      }, false);
      
    }
   
    initialize() {
      var spy = new Gumshoe('header .nav-menu a', {
        offset: function () {
          return document.querySelector('header.main-head').getBoundingClientRect().height +  (window.innerHeight / 2.5);
        },
        // Active classes
        navClass: 'active', // applied to the nav list item
        contentClass: 'active', // applied to the content
      
        // Nested navigation
        nested: true, // if true, add classes to parents of active link
        nestedClass: 'active', // applied to the parent items
      
        // Offset & reflow
        // offset: -70, // how far from the top of the page to activate a content area
        reflow: true, // if true, listen for reflows
      
        // Event support
        events: true // if true, emit custom events
      
      });
   
        if (!this.host.querySelector("li.active")){ 
          addClass(this.host.querySelector(".nav-menu ul li:first-child"), 'active');
        }
    }
    position_indicator(ele){
      
      let indicator = document.querySelector('.menu-ink');
      let left = ele.offsetLeft - 10;
      let width = ele.offsetWidth - 34;
      indicator.style.left = left+'px'; 
      indicator.style.width = width+'px';
    }
}