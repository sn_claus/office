import Component from '../components';

export default class Ink extends Component {
    static get selector() {
      return '.btn';
    }

    constructor(host) {
        super(host);
         var links = document.querySelectorAll('.btn');
    
        for (var i = 0, len = links.length; i < len; i++) {
            links[i].addEventListener('click', function(e) {
            var _offsetX = e.offsetX;
            var _offsetY = e.offsetY;
            var targetEl = e.target;
            var inkEl = targetEl.querySelector('.ink');
        
            if (e.target.querySelector('.ink')) {
                inkEl = e.target.querySelector('.ink')
                inkEl.classList.remove('animate');
            }
            else {
                inkEl = document.createElement('span');
                inkEl.classList.add('ink');
                inkEl.style.width = inkEl.style.height = Math.max(targetEl.offsetWidth, targetEl.offsetHeight) + 'px';
                e.target.appendChild(inkEl);
            }
            if(!e.offsetX) {
                _offsetX =  e.target.querySelector('.ink').offsetWidth / 2;
                _offsetY = e.target.querySelector('.ink').offsetHeight / 2;
            }
            e.target.querySelector('.ink').style.left = (_offsetX - e.target.querySelector('.ink').offsetWidth / 2) + 'px';
            e.target.querySelector('.ink').style.top = (_offsetY - e.target.querySelector('.ink').offsetHeight / 2) + 'px';
            e.target.querySelector('.ink').classList.add('animate');
            }, false);
        }
        if(document.querySelector('.btn-play')) {
            document.querySelector('.btn-play').addEventListener('click', function(e) {
                if(e.target.classList.contains('btn-play')) {
                    this.querySelector('.play-wrap').click();
                }
            })
        }
    }
       
    initialize() {
;
    }
  }
  