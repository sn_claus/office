import Component from '../components';
import MicroModal from 'micromodal';
import hasClass from 'dom-helpers/class/hasClass';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';

export default class Modal extends Component {
    static get selector() {
      return '.micro_modal';
    }
    constructor(host) {
      super(host);
      this.config = {
        disableScroll: true,
        disableFocus: true,
        awaitCloseAnimation: true,
        // debugMode: true,
        onShow: modal => {
            // modal.find('iframe');
            console.log('fired');
            if(hasClass(modal, 'autoplay')) {
              this.iframe = modal.querySelector('iframe');
              if(!hasClass(this.iframe, 'played')) {
                this.iframe.src += "&autoplay=1";
                addClass(this.iframe, 'played');
              }
          }
        },
        onClose: modal => {
          if(hasClass(modal, 'autoplay')) {
            this.src = this.iframe.src;
            this.src = this.src.replace('&autoplay=1','');
            removeClass(this.iframe, 'played');
            this.iframe.setAttribute('src', this.src);
          }
        }
      };
    }
    
    initialize() {
      MicroModal.init(this.config);
    }
  }

