import Component from '../components';
import createEventBus from '../eventBus';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';

export default class Nav extends Component {
  static get selector() {
    return '.main-nav';
  }
  static set siteMenuActive(value=false) {
    _siteMenuActive = value;
  }
  static set eventBus(value=null) {
    _eventBus = value;
  }
  
  constructor(host) {
    super(host);
    this.trigger = document.querySelector('.nav-menu-toggler button');
    this.mainNav = document.querySelector('footer .nav-mobile');
    this.createEvent('.nav-menu-toggler', 'click', this.toggleMenu.bind(this));
    this.links = document.querySelectorAll('.nav-mobile a');
    for (var i = 0, len = this.links.length; i < len; i++) {
      this.links[i].addEventListener('click', function(event) {
        document.querySelector('.nav-menu-toggler button').click();
      });
    }
  }
  
  
  /**
   * Toggle header's theme color
   * @param {string} theme Theme name
   */

  /**
   * Open/close site menu
   */
  toggleMenu() {
    if (this.siteMenuActive) {
      removeClass(this.trigger, 'opened');
      removeClass(document.body, 'nav-open');
      removeClass(this.host, 'is-site-menu-active');
      removeClass(this.mainNav, 'is-sub-nav-active');
    } else {
      addClass(this.trigger, 'opened');
      addClass(this.host, 'is-site-menu-active');
      addClass(document.body, 'nav-open');
    }

    this.siteMenuActive = !this.siteMenuActive;
  }

  initialize() {
    this.eventBus = createEventBus(true);
  }
}
