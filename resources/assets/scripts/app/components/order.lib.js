import Scrollbar from 'smooth-scrollbar';
import Component from '../components';
import createEventBus from '../eventBus';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import lozad from 'lozad';

export default class Order extends Component {
    static get selector() {
        return 'body';
      }
  
    constructor(host) {
        super(host);
        this.mainBtn = this.find('#submit-order');
        this.hidden = this.find('#order input[name="your-select"]');
        this.term = this.find("#checkbox-1");
        this.observer = lozad('#order .lazyload');
        this.Scrollbar = this.find('#order .modal-content');
        this.action_btn = this.find('section.offers .btn');
        this.title = this.find("#order .part-title");
        this.update = this.find("#order .dropdown-item");
        console.log(this.update);
        this.orderImg = this.find("#order .lazyload");
        this.createEvent(this.action_btn, 'click', this.order_form.bind(this));
        this.createEvent(this.update, 'click', this.update_choice.bind(this));
        this.createEvent(this.term, 'change', this.disable.bind(this));
    
    }
      order_form(){
        this.choice = event.target.dataset.choice; // choice;
        this.hidden.value = this.choice;
        this.img = this.find('img[data-imgindex="'+this.choice+'"]');
        this.select = this.find('button[data-title="'+this.choice+'"]').click();
        this.title.innerText = this.choice;
        this.orderImg.dataset.src = this.img.src;
        this.orderImg.dataset.loaded = 'false';
        this.observer.observe();
        console.log(this.img.src);
        if(!scroll) {
            var scroll = Scrollbar.init(this.Scrollbar);
            this.scrollto(scroll);
        } 
        else {
           this.scrollto(scroll);
        }
      }
      update_choice() {
        this.hidden.value = this.choice;
        var choice = event.target.dataset.title; // choice;
        console.log(event);
        this.title.innerText = choice;
        var img = this.find('img[data-imgindex="'+choice+'"]');
        this.orderImg.dataset.src = img.src;
        this.orderImg.dataset.loaded = 'false';
        this.observer.observe();
        
      }
      disable(){
        console.log(event);
        if(event.target.checked) {
            this.mainBtn.disabled = false;
        }
        else {
            this.mainBtn.disabled = true;
        }
      }
      scrollto(el){
        el.scrollIntoView(this.find('#submit-order'),{
            alignToTop: true
        });
      }
      initialize() {
        
      }
}