import Component from '../components';
import createEventBus from '../eventBus';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import {MDCFormField} from '@material/form-field';
import {MDCRadio} from '@material/radio';

export default class Radio extends Component {
    static get selector() {
        return '.radio-group';
      }
      
      constructor(host) {
        super(host);
        const radio = new MDCRadio(this.host.querySelector('.mdc-radio'));
        const formField = new MDCFormField(this.host.querySelector('.mdc-form-field'));
        formField.input = radio;
      }
      initialize() {

      }
}