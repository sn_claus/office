import Component from '../components';
import createEventBus from '../eventBus';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';

export default class Scroll extends Component {
    static get selector() {
        return 'a[href^="#"]';
      }
  
      constructor(host) {
        super(host);
        this.createEvent(this.host, 'click', function(e){
            e.preventDefault();
            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
      }
}