import Component from '../components';
// import "swiper/dist/css/swiper.min.css";
import Swiper from 'swiper/dist/js/swiper.js';


export default class swiper_partners extends Component {
  static get selector() {
    return '.offers-content';
  }

  constructor(host) {
    super(host);

    this.mobile_only = undefined;
    this.container = document.querySelector('.swiper-container');
    this.media = window.matchMedia("(max-width: 991px)");
    // this.createEvent(window, 'onresize', this.updateSwiper.bind(this));
    window.onresize = this.updateSwiper.bind(this);
    
      // navigation: {
      //   prevEl: this.arrowPrev,
      //   nextEl: this.arrowNext
      // }
    }
    updateSwiper() {
      if(this.media.matches && this.mobile_only == undefined) {            
        this.mobile_only = new Swiper('.swiper-container', {            
            slidesPerView: 1,
            spaceBetween: 15,
            loop: false,
            pagination: {
              el: '.swiper-pagination',
            },
        });
    } else if (!this.media.matches && this.mobile_only != undefined) {
      this.mobile_only.destroy();
      this.mobile_only = undefined;
      this.container.removeAttribute("style");     
    } 
    }


  initialize() {
    this.updateSwiper();
  }
}