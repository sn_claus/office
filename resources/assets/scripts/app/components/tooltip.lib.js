import Component from '../components';
import tippy from 'tippy.js';


export default class Tooltip extends Component {
    static get selector() {
      return '.tooltip-bundle, .dropdown-fancy';
    }

    constructor(host) {
        super(host);
        this.btns = this.find('button, .btn-info');
    }
       
    initialize() {
        tippy(this.btns, {
            trigger: 'click',
            interactive: true,
            arrow: true,
            animation: 'scale',
            inertia: true,
            theme: 'custom',
            content(reference) {
              const id = reference.getAttribute('data-template')
              const container = document.createElement('div')
              const linkedTemplate = document.getElementById(id)
              const node = document.importNode(linkedTemplate.content, true)
              container.appendChild(node)
              return container
            },
          });
    }
  }
  