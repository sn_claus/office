import Component from '../components';
import createEventBus from '../eventBus';
import addClass from 'dom-helpers/class/addClass';
import hasClass from 'dom-helpers/class/hasClass';
import removeClass from 'dom-helpers/class/removeClass';

export default class Valid extends Component {
    static get selector() {
        return '.wpcf7-form-control-wrap';
      }
      
      constructor(host) {
        super(host);
        this.input = this.find('.wpcf7-form-control');
        this.createEvent(this.input, 'focus', this.allclear.bind(this));
      }
      allclear() {
        if(hasClass(this.input, 'wpcf7-not-valid')) {
            alert = this.find('span[role="alert"]');
            removeClass(this.input, 'wpcf7-not-valid');
            this.host.removeChild(alert);
        }
      }
      initialize() {

      }
}