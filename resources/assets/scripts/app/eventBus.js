import EventEmitter from 'events';

let instance = null;

/**
 * Create event bus for communicate between different parts
 * of an application
 * @param {boolean} global - Return singleton or new istance
 * @returns {EventEmitter}
 */
export default function createEventBus(global) {
  if (global) {
    if (!instance) {
      instance = new EventEmitter();
    }

    return instance;
  } else {
    return new EventEmitter();
  }
}
