import toArray from 'lodash/toArray';
import debounce from 'lodash/debounce';
import addClass from 'dom-helpers/class/addClass';
import removeClass from 'dom-helpers/class/removeClass';
import hasClass from 'dom-helpers/class/hasClass';
import createEventBus from './eventBus';

export default class ScrollBehavior {
  constructor() {
    this.header = document.querySelector('header');
    this.themeSections = toArray(document.querySelectorAll('[data-theme]'));
    this.scrollTopAnchor = document.querySelector('.scroll-top-anchor');
    this.scrollPos = 0;
    this.eventBus = createEventBus(true);

    this.toggleHeaderTheme = this.toggleHeaderTheme.bind(this);
    this.toggleScrollTopAnchor = this.toggleScrollTopAnchor.bind(this);
    this.scrollToTop = this.scrollToTop.bind(this);

    this.initialize();
  }

  /**
   * Detect if section in viewport
   * @param {HTMLElement} section
   * @returns {boolean}
   */
  themeSectionInViewport(section) {
    const headerHeight = this.header.offsetHeight;
    const viewportTop = scrollY || pageYOffset;
    const sectionTop = section.offsetTop - headerHeight / 2;
    const sectionBottom = sectionTop + section.offsetHeight;

    return viewportTop >= sectionTop && viewportTop <= sectionBottom;
  }

  /**
   * Toggle header theme to light/dark relative to active section currently in viewport
   */
  toggleHeaderTheme() {
    this.themeSections.forEach(section => {
      const headerToggled = hasClass(section, 'is-theme-changed');
      const inViewport = this.themeSectionInViewport(section);

      if (inViewport && !headerToggled) {
        const theme = section.dataset.theme;
        addClass(section, 'is-theme-changed');
        this.eventBus.emit('header:toggleTheme', theme);
      } else if (!inViewport && headerToggled) {
        removeClass(section, 'is-theme-changed');
      }
    });

    const viewportTop = scrollY || pageYOffset;

    if (viewportTop >= innerHeight && !hasClass(this.header, 'bg')) {
      addClass(this.header, 'bg');
      
    } else if (viewportTop < innerHeight && hasClass(this.header, 'bg')) {
      removeClass(this.header, 'bg');
      removeClass(this.header, 'hide');
    }

    // if ((document.body.getBoundingClientRect()).top > this.scrollPos) {
    //   removeClass(this.header, 'hide'); //up
    // }
    // else if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
    //   removeClass(this.header, 'hide'); //endn
    // }
    // else if ((window.innerHeight + window.scrollY) < document.body.offsetHeight) {
    //   addClass(this.header, 'hide');//down
    // }
    
    // this.scrollPos = (document.body.getBoundingClientRect()).top;
    // console.log(this.scrollPos);
  }

  /**
   * Navigate to the top of the page
   */
  scrollToTop() {
    const scrollDuration = 800;
    const cosParameter = scrollY / 2;

    let scrollCount = 0;
    let oldTimestamp = performance.now();

    function step(newTimestamp) {
      scrollCount += Math.PI / (scrollDuration / (newTimestamp - oldTimestamp));

      if (scrollCount >= Math.PI) window.scrollTo(0, 0);
      if (window.scrollY === 0) return;

      scrollTo(
        0,
        Math.round(cosParameter + cosParameter * Math.cos(scrollCount))
      );

      oldTimestamp = newTimestamp;
      requestAnimationFrame(step);
    }

    requestAnimationFrame(step);
  }

  /**
   * Toggle scrollTopAnchor visibility
   */
  toggleScrollTopAnchor() {
    if (!this.scrollTopAnchor) return;

    const viewportTop = scrollY || pageYOffset;
    const visible = hasClass(this.scrollTopAnchor, 'is-visible');

    if (viewportTop >= innerHeight && !visible) {
      addClass(this.scrollTopAnchor, 'is-visible');
    } else if (viewportTop <= innerHeight && visible) {
      removeClass(this.scrollTopAnchor, 'is-visible');
    }
  }

  initialize() {
    if (this.scrollTopAnchor) {
      this.scrollTopAnchor.addEventListener('click', this.scrollToTop);
    }

    const debouncedToggle = debounce(() => {
      this.toggleHeaderTheme();
      this.toggleScrollTopAnchor();
    }, 10);

    window.addEventListener('resize', debouncedToggle);
    window.addEventListener('scroll', debouncedToggle);

    debouncedToggle();
  }
}
