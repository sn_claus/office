import AOS from 'aos'; // Scroll animation
import ScrollBehavior from './scrollBehavior';

export default function windowLoad() {
  window.addEventListener('load', () => {
    const sb = new ScrollBehavior();
    AOS.init({disable: 'mobile',});
  });
}
