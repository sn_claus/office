<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Template &rsaquo; Error', 'sage');
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the template directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);
class web_walker extends Walker_Nav_Menu {

		private $show_caret;
		private $target_device; //мобильное меню ('mobile')?

		function __construct($show_caret = true, $target_device=false ) { // в конструкторе
			//$this->open_submenu_on_hover = $open_submenu_on_hover; // запишем параметр раскрывания субменю
			$this->show_caret = $show_caret;
			$this->target_device = $target_device;
	    }

		function start_lvl(&$output, $depth = 0, $args = array()) { // старт вывода подменюшек
			$output .= "\n<ul class=\"nav-dropdown\">\n"; // ул с классом
		}
		function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) { // старт вывода элементов
			$item_html = ''; // то что будет добавлять
			parent::start_el($item_html, $item, $depth, $args); // вызываем стандартный метод родителя
			if ( $item->is_dropdown && $depth === 0 ) { // если элемент содержит подменю и это элемент первого уровня
			   if ($this->target_device === 'mobile') $item_html = str_replace('<a', '<button class="dropdown_toggle mobile fa fa-caret-down" role="button"></button> <a', $item_html); // если подменю не будет раскрывать при наведении надо добавить стандартные атрибуты бутстрапа для раскрытия по клику
			   else $item_html = str_replace('</a>', ' <b class="caret"></b></a>', $item_html); // это стрелочка вниз
            }
            // if($item->current) {
            //     $item_html = str_replace('<a href="', '<span data-url="', $item_html);
            //     $item_html = str_replace('</a>', '</span>', $item_html);
            // }
			$output .= $item_html; // приклеиваем теперь
		}
		function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) { // вывод элемента
			if ( $element->current ) $element->classes[] = 'active'; // если элемент активный надо добавить бутстрап класс для подсветки
			$element->is_dropdown = !empty( $children_elements[$element->ID] ); // если у элемента подменю
			if ( $element->is_dropdown ) { // если да
			    if ( $depth === 0 ) { // если li содержит субменю 1 уровня
			        $element->classes[] = 'dropdown'; // то добавим этот класс
			        //if ($this->open_submenu_on_hover) $element->classes[] = 'show-on-hover'; // если нужно показывать субменю по хуверу
			    } elseif ( $depth === 1 ) { // если li содержит субменю 2 уровня
			        $element->classes[] = 'dropdown-submenu'; // то добавим этот класс, стандартный бутстрап не поддерживает подменю больше 2 уровня по этому эту ситуацию надо будет разрешать отдельно
			    }
			}
			parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output); // вызываем стандартный метод родителя
		}
    }
    function get_excerpt($limit, $source = null) {

        if ($source == "post_content" ? ($excerpt = get_the_content()) : ($excerpt = $source))
            $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);
        $excerpt = strip_shortcodes($excerpt);
        $excerpt = strip_tags($excerpt);
        $excerpt = mb_substr($excerpt, 0, $limit);
    
        if ($limit === mb_strlen($excerpt)) {
            $excerpt = $excerpt . '...';
        }
        /* $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
          $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt)); */
    
        return $excerpt;
    }
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page('Theme Settings');    
    }
    add_filter('wp_get_attachment_image_attributes', function ($attr, $attachment) {
        // Bail on admin
        if (is_admin()) {
            return $attr;
        }
    
        $attr['data-src'] = $attr['src'];
        $attr['class'] .= ' lazyload';
        unset($attr['src']);
    
        return $attr;
    }, 10, 2);
    add_filter('sage/template/page/data', function (array $data) {
        $data['current_lang'] = pll_current_language('name');
        $data['langs'] = pll_the_languages(array('raw'=> 1 ));
        return $data;
    });
    function register_my_menus() {
        register_nav_menus(
          array(
            'additional' => __( 'primary #2' )
          )
        );
      }
      add_action( 'init', 'register_my_menus' );

    pll_register_string('default', 'Your Virtual');
    pll_register_string('default', 'Office');
    pll_register_string('default', 'Your Virtual office');
    pll_register_string('default', 'The flexible solution');
    pll_register_string('default', 'See Offer');
    pll_register_string('default', 'Play the video');
    pll_register_string('default', 'Best choice');
    pll_register_string('default', 'Order');
    pll_register_string('default', 'Tax Calculator');
    pll_register_string('default', 'Commercial income');
    pll_register_string('default', 'Allowances');
    pll_register_string('default', 'Sole proprietorship / partnerships');
    pll_register_string('default', 'Corporations');
    pll_register_string('default', 'Legal persons of public law e.g. Clubs');
    pll_register_string('default', 'Send');
    pll_register_string('default', 'Trade tax calculation');
    pll_register_string('default', 'Individual companies / partnership');
    pll_register_string('default', 'Trade Tax Base');
    pll_register_string('default', 'Trade Tax Base Rate');
    pll_register_string('default', 'Trade tax assesment value');
    pll_register_string('default', 'Trad tax payable so far');
    pll_register_string('default', 'Results in trade tax');
    pll_register_string('default', 'Trade tax burden so far');    
    pll_register_string('default', 'Trad tax with OfficeNow');
    pll_register_string('default', 'contact us');
    pll_register_string('default', 'month');
    pll_register_string('default', 'more information');
    pll_register_string('default', 'Save business tax');
    pll_register_string('default', 'offers');
    pll_register_string('default', 'Fits perfectly');
    pll_register_string('default', 'about us');
    pll_register_string('default', 'what we do');
    pll_register_string('default', 'Our Service');
    pll_register_string('default', 'tax');
    pll_register_string('default', 'Trade tax burden OfficeNOW');
    pll_register_string('default', 'Trade tax rate OfficeNOW');
    pll_register_string('default', 'Trade tax rate so far');
    pll_register_string('default', 'Trade tax rate');
    pll_register_string('default', 'Trade tax base');
