@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-12 text-center">
      <h2 class="bit-title">404</h2>
      <h1>Your office is not found yet</h1>
    <a href="{!!get_home_url()!!}" class="btn light">go to office</a>
    </div>
  </div>
</div>
@endsection
