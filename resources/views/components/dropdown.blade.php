{{-- mixin @dropdown($name, $placeholder, $items) --}}

<div class="dropdown">
    <input type="hidden" name={{ $name }}>
    <button class="dropdown-toggle" type="button">
        <span class="choice">{{$placeholder}}</span>
        {{-- @icon('angle-up')
        @endicon --}}
    </button>
    <div class="dropdown-menu position-absolute d-none scroll-container transparent">
        @foreach ($items as $item)
            <button class="dropdown-item" type="button" data-value={{$item}}>{{$item}}</button>
        @endforeach
    </div>
</div>