{{-- mixin @icon($name) --}}
<svg class="icon icon-{{$name}}" width="" >
    <use xlink:href="@asset(images/sprite_icons.svg#{{$name}})">
</svg>