{{-- mixin @langswitcher($name, $placeholder, $items) --}}

<div class="dropdown">
    <input type="hidden" name={{ $name }}>
    <button class="dropdown-toggle btn light small" type="button">
        <span class="choice">{{$placeholder}}</span>
        {{-- @icon('angle-up')
        @endicon --}}
    </button>
    <div class="dropdown-menu position-absolute d-none scroll-container transparent">
        @foreach ($items as $item)
            @if($placeholder == $item['name'])
                <span class="dropdown-item active dissabled">{{$item['name']}}</span>
            @else
                <a href={{$item['url']}} class="dropdown-item" data-value={{$item['name']}}>{{$item['name']}}</a>
            @endif
        @endforeach
    </div>
</div>