{{-- mixin @section-title($title, $subtitle) --}}
<div class="section-title">
    <div class="aos-controller">
        <span class="subtitle d-block" data-aos="slide-up" data-aos-duration="800" data-aos-delay="800" data-aos-once="true">{{pll__($subtitle)}}</span>
    </div>
        @if($icon)
            <div class="maintitle" data-aos>
                <img src="#" data-aos="fade-up" data-aos-duration="800" data-aos-delay="200" data-aos-once="true" class="img-fluid lazyload" data-src="@asset('images/logo.png')">
            </div>
        @else
            <div class="aos-controller">
                <h2 class="maintitle" data-aos="slide-up" data-aos-duration="800" data-aos-delay="200" data-aos-once="true">{{pll__($title)}}</h2>
            </div>
        @endif
</div>