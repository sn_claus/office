{{--
  Template Name: home template
--}}

@extends('layouts.app')
@section('content')
@while(have_posts()) @php the_post();  @endphp
  @include('partials.home.hero')
  @include('partials.home.our-service')
  @include('partials.home.about')
  @include('partials.home.offers')
  @include('partials.home.tax')
  @include('partials.home.contact')
@endwhile

  {{-- @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')
  @endwhile --}}
@endsection
