@extends('layouts.app')

@section('content')
<div class="single-page">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center mb-5 pb-3">
        <h1>{!!get_the_title()!!}</h1>
      </div>
      @while(have_posts()) @php the_post() @endphp
        <div class="col-12">
          {!!get_the_content()!!}
        </div>
      @endwhile
    </div>
  </div>
</div>
@endsection
