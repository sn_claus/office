<footer>
  <div class="container">
    <div class="row foot-links">
        <div class="col-auto">
            @if($current_lang === "English")
              <a href="{!!get_permalink(220)!!}">Imprint</a>
              <a href="{!!get_permalink(245)!!}">Terms and Conditions</a>
              <a href="{!!get_permalink(222)!!}">Data Protection</a>
            @else
              <a href="{!!get_permalink(112)!!}">Impressum</a>
              <a href="{!!get_permalink(242)!!}">Allgemeine Geschäftsbedingungen</a>
              <a href="{!!get_permalink(225)!!}">Datenschutz</a>
            @endif
        </div>
    </div>
    <div class="row row-copy">
      <div class="col-12 text-center">
        {{-- <span>© Copyright 2019{!! get_field('copy', 'options') ? ' - '.get_field('copy', 'options') : '' !!}</span> --}}
      </div>
    </div>
  </div>
  <div class="nav-mobile">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'items_wrap' => '<ul id="%1$s" class="nav %2$s">%3$s</ul>', 'container'=> false,'walker' => new web_walker]) !!}
      @endif
      <div class="logo-wrap d-none d-md-block">
        <img alt="logo-mobile" class="logo-img" src="<?= get_template_directory_uri(); ?>/assets/images/logo_white.png">
      </div>
  </div>
  <div class="other-stuff">
    <div class="micro_modal fullscreen-modal autoplay" id="play" aria-hidden="true">
      <div class="modal_overlay d-flex justify-content-center align-items-center" tab-index="-1" data-micromodal-close>
        <div class="modal_container" role="dialog" aria-modal="true" aria-labelledby="play">
          <header class="modal-header d-flex justify-content-start align-items-center">
            <button class="modal-close toggle-nav opened" aria-label="Close modal" data-micromodal-close >
                <span class="shape d-none"></span>
                <span class="shape"></span>
                <span class="shape"></span>
                <span class="shape d-none"></span>
            </button>
          </header>
            <div class="modal-content">
              {!!$about_us->link!!}
            </div>
          </div>
        </div>
    </div>
    <div class="micro_modal big-modal" id="calculate" aria-hidden="true">
        <div class="modal_overlay d-flex justify-content-center align-items-center" tab-index="-1" data-micromodal-close>
          <div class="modal_container" role="dialog" aria-modal="true" aria-labelledby="calculate">
            <header class="modal-header d-flex justify-content-start align-items-center">
              <h3 class="part-title v2">{{pll__('Trade tax calculation')}}</h3>
              <button class="modal-close toggle-nav opened" aria-label="Close modal" data-micromodal-close >
                  <span class="shape d-none"></span>
                  <span class="shape"></span>
                  <span class="shape"></span>
                  <span class="shape d-none"></span>
              </button>
            </header>
              <div class="modal-content">
                <div class="modal-form form-init simulate">
                  <div class="form-group">
                      <label class="lab-wrap" for="calc_inp">
                          {{pll__('Commercial income')}} €
                      </label>
                      <input class="form-control" name="commercial" id="calc_inp">
                  </div>
                  <div class="form-group">
                      <label class="lab-wrap" for="calc_outp1">
                          {{pll__('Individual companies / partnership')}}
                      </label>
                      <input class="form-control" name="commercial" id="calc_outp1" disabled>
                  </div>
                  <div class="form-group">
                      <label class="lab-wrap" for="calc_outp2">
                          {{pll__('Trade tax base')}}
                      </label>
                      <input class="form-control" name="commercial" id="calc_outp2" disabled>
                  </div>
                  <div class="form-group multiply">
                    <div class="form-group">
                      <label class="lab-wrap" for="trade_outp1">
                          {{pll__('Trade Tax Base Rate')}}
                      </label>
                      <input class="form-control" name="commercial" value="3,5%" id="trade_outp1" disabled>
                    </div>
                    <div class="form-group">
                        <label class="lab-wrap" for="result_outp1">
                            {{pll__('Trade tax assesment value')}}
                        </label>
                        <input class="form-control" name="commercial" id="result_outp1" disabled>
                      </div>
                  </div>
                  <div class="form-group multiply">
                    <div class="form-group">
                      <label class="lab-wrap" for="trade_outp2">
                          {{pll__('Trade tax rate so far')}}
                      </label>
                      <input class="form-control" name="commercial" value="490%"  id="trade_outp2" disabled>
                    </div>
                    <div class="form-group">
                        <label class="lab-wrap" for="result_outp2">
                            {{pll__('Trade tax burden so far')}}
                        </label>
                        <input class="form-control" name="commercial" id="result_outp2" disabled>
                      </div>
                  </div>
                  <div class="form-group multiply">
                      <div class="form-group">
                        <label class="lab-wrap" for="trade_outp3">
                            {{pll__('Trade tax rate OfficeNOW')}}
                        </label>
                        <input class="form-control" name="commercial" value="240%"  id="trade_outp3" disabled>
                      </div>
                      <div class="form-group">
                          <label class="lab-wrap" for="result_outp3">
                              {{pll__('Trade tax burden OfficeNOW')}}
                          </label>
                          <input class="form-control" name="commercial" id="result_outp3" disabled>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      <div class="micro_modal big-modal" id="order" aria-hidden="true">
        <div class="modal_overlay d-flex justify-content-center align-items-center" tab-index="-1" data-micromodal-close>
          <div class="modal_container" role="dialog" aria-modal="true" aria-labelledby="order-title">
            <header class="modal-header d-flex justify-content-start align-items-center">
              <h3 class="part-title v2" id="order-title">123</h3>
              <button class="modal-close toggle-nav opened" aria-label="Close modal" data-micromodal-close >
                  <span class="shape d-none"></span>
                  <span class="shape"></span>
                  <span class="shape"></span>
                  <span class="shape d-none"></span>
              </button>
            </header>
              <div class="modal-content">
                <div class="modal-form form-init simulate">
                    <div class="img-wrap">
                      <img src="#!" data-src="#!" class="lazyload img-fluid mx-auto" alt="order img">
                      <div class="swiper-lazy-preloader"></div>
                    </div>
                      @if($current_lang === "English")
                        {!! do_shortcode('[contact-form-7 id="17" title="order(en)"]') !!}
                      @else {!! do_shortcode('[contact-form-7 id="85" title="order(de)"]'); !!}
                      @endif
                </div>
                </div>
              </div>
            </div>
          </div>
          <div class="mdc-snackbar mdc-snackbar--leading">
              <div class="mdc-snackbar__surface">
                <div class="mdc-snackbar__label" role="status" aria-live="polite">
                </div>
              </div>
            </div>
      </div>
</footer>