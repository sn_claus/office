<header class="main-head">
  <div class="container">
  <nav class="main-nav">
    <div class="logo-wrap">
        <img alt="logo-main" class="logo-img" src="@asset('images/logo_white.png')">
    </div>
    <div class="nav-menu-toggler d-md-none">
        <button type="button" class="toggle-nav">
          <span></span>
          <span class="less"></span>
          <span class="less"></span>
          <span></span>
        </button>
      </div>
    <div class="nav nav-menu d-none d-md-block">
      @if (has_nav_menu('primary_navigation') && is_front_page())
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'walker' => new web_walker]) !!}
      @elseif (has_nav_menu('additional'))
        {!! wp_nav_menu(['theme_location' => 'additional', 'menu_class' => 'nav', 'walker' => new web_walker]) !!}
      @endif
    </div>
    <div class="nav lang-switcher d-none d-md-block">
      @component('components.langswitcher', ['name' => 'langswitcher','placeholder' => $current_lang, 'items' => $langs])
      @endcomponent
    </div>
  </nav>
</div>
</header>
