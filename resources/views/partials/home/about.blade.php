<section class="about-us" id="about_us">
    @title(['subtitle' => 'about us', 'icon' => true])
    @endtitle
    <div class="about-content">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-md-6 col-12 about-column">
                    <div data-aos="fade-left" data-aos-duration="800" data-aos-delay="600" data-aos-once="true">{!!$about_us->text!!}</div>
                    @if ($about_us->link)
                        <button data-aos="fade" data-aos-duration="800" data-aos-delay="800" data-aos-once="true" class="btn-play no-btn my-auto" data-micromodal-trigger="play">
                            <div class="play-wrap btn">
                                <img src="#" alt="play" class="img-fluid lazyload" data-src="@asset('images/play.png')">  
                                <div class="swiper-lazy-preloader"></div>
                            </div>
                            <span class="text-btn">{{pll__('Play the video')}}</span> 
                        </button>
                    @endif
                </div>
                <div class="col-xl-7 col-md-6 col-12">
                    <img src="#" data-aos="fade-right" data-aos-duration="800" data-aos-delay="200" data-aos-once="true" class="img-fluid lazyload" alt="about-us" data-src="@asset('images/about_us.png')">
                    <div class="swiper-lazy-preloader"></div>
                </div>
            </div>
        </div>
    </div>
</section>