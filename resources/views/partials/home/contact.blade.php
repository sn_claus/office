@php $phone = preg_replace('/\D/', '', $contacts->phone) @endphp
<section class="contact-us" id="contact_us">
        @title(['title' => 'contact us','subtitle' => 'more information'])
        @endtitle
    <div class="contact-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-xl-3 off-set-xl-1 col-12">
                    <img src="#" class="lazyload img-fluid" data-aos="fade-up" data-aos-duration="800" data-aos-delay="400" data-aos-once="true" data-src="@asset('images/logo.png')" alt="logo">
                    <div class="contacts-info">
                        <div class="single-info" data-aos="fade-up" data-aos-duration="800" data-aos-delay="600" data-aos-once="true">
                            <img src="#" data-src="@asset('images/place.png')" class="img-fluid lazyload" alt="contact-icon">
                            <div class="swiper-lazy-preloader"></div>
                            <span>{!!$contacts->place!!}</span>
                        </div>
                    <a class="single-info" href="tel:{{$phone}}" data-aos="fade-up" data-aos-duration="800" data-aos-delay="800" data-aos-once="true">
                            <img src="#" data-src="@asset('images/phone.png')" class="img-fluid lazyload" alt="contact-icon">
                            <div class="swiper-lazy-preloader"></div>
                            <span>{!!$contacts->phone!!}</span>
                        </a>
                        <a class="single-info" href="mailto:{!!$contacts->mail!!}" data-aos="fade-up" data-aos-duration="800" data-aos-delay="1000" data-aos-once="true">
                            <img src="#" data-src="@asset('images/mail.png')" class="img-fluid lazyload" alt="contact-icon">
                            <div class="swiper-lazy-preloader"></div>
                            <span>{!!$contacts->mail!!}</span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-6 offset-lg-1 offset-xl-2 col-12">
                    <div class="form-body form-init row">
                        @if($current_lang === "English")
                            {!! do_shortcode('[contact-form-7 id="87" title="feedback(en)"]') !!}
                        @else {!! do_shortcode('[contact-form-7 id="88" title="feedback(de)"]'); !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
</section>