<section class="offers" id="offers">
    @title(['title' => 'offers', 'subtitle' => 'Fits perfectly'])
    @endtitle
    <div class="offers-content">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center" data-aos="fade-up" data-aos-duration="800" data-aos-delay="400" data-aos-once="true">
                    {!!$offers->text!!}
                </div>
            </div>
            <div class="row">
                <div class="col-xl-10 offset-xl-1 col-12 swiper-container">
                    <div class="row swiper-wrapper" data-aos="columns" data-aos-once="true">
                    @foreach ($offers->items as $key => $item )
                        <div class="item col-lg-4 col-12 swiper-slide" data-aos="fade-left" data-aos-duration="800" data-aos-delay={{$key*400}} data-aos-once="true">
                            <div class="item-wrap">
                                @if($item['best'])
                                    <div class="item-badge" data-aos data-aos-delay="4s">
                                        <div class="badge-wrapper">
                                            <span class="front">
                                                <span class="content">{{pll__('Best choice')}}</span>
                                            </span>
                                            <span class="back"></span>
                                        </div>
                                    </div>
                                @endif
                                <h3 class="item-title">{{$item['title']}}</h3>
                                <div class="img-wrap">
                                    <img src="#" data-imgindex="{{$item['title']}}" data-src="{{$item['img']}}" class="img-fluid lazyload item{!!$key!!}" alt="offer item">
                                    <div class="swiper-lazy-preloader"></div>
                                </div>
                                <div class="price-group">
                                    <span class="currency">€</span>
                                    <span class="value">{!!$item['price']!!}</span>
                                    <span class="month">/ {{pll__('month')}}</span>
                                </div>
                                <div class="item-feature">
                                    @foreach ($item['features'] as $feature)
                                    <div class="single-feature">
                                        @if($feature['check'])
                                            <img src="#" alt="true" class="lazyload img-fluid" data-src="@asset('images/true.png')">
                                        @else
                                            <img src="#" alt="false" class="lazyload img-fluid" data-src="@asset('images/false.png')">
                                        @endif
                                        <span>{!!$feature['title']!!}</span>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="item-action">
                                    <button class="btn blue" data-choice="{{$item['title']}}" data-micromodal-trigger="order">{{pll__('Order')}}</button>
                                </div>
                            </div>
                            
                        </div>
                    @endforeach
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
</section>