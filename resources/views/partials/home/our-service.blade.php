<section class="our-service" id="our_service">
    @title(['title' => 'Our Service', 'subtitle' => 'what we do'])
    @endtitle
    <div class="service-items">
        <div class="container">
            <div class="row">
                @foreach ($our_service as $key => $item)
                    <div class="item col-xl-3 col-6" data-aos="fade" data-aos-duration="800" data-aos-delay={{($key+1)*400}} data-aos-once="true">
                        <div class="item-img">
                            <img class="img-fluid lazyload" src="#" data-src="{{$item->icon}}">
                            <div class="swiper-lazy-preloader"></div>
                        </div>
                        <h3 class="item-title">{{$item->title}}</h3>
                        <span class="item-desc">{{$item->text}}</span>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>